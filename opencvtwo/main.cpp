#include <opencv/highgui.h>
#include <opencv/cv.h>
#include <vector>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

using namespace cv;
using namespace std;

/// Global variables
Mat src, dst, tmp,d1,grey;



int main( int argc, char** argv )
{

    Ptr<FeatureDetector> fd = FeatureDetector::create("FAST");
    vector<KeyPoint> keypoints;
    
    VideoCapture a("/Users/home/Desktop/raw/quad2.mp4");
    
    if (!a.isOpened())
    {
        std::cout<<"ERROR OPENING!";
    }
    else std::cout<<"YO DAWG, it opened!";


    Mat edges ;
    
    while (1)
    {
        Mat frame;
        a>>frame;
        
        edges = frame;
//        cvtColor(frame, edges, CV_BGR2GRAY);
        
        fd->detect(edges, keypoints);
        
        Point pt;
        Scalar sc = Scalar(255,255,255);
        
        for (int i=0; i<keypoints.size(); i++)
        {
//            cout<<(keypoints[i].pt).x<<endl;
            pt.x =(keypoints[i].pt).x;
            pt.	y =(keypoints[i].pt).y;
            circle(edges, pt, 5, sc);
            
            
        }
        cout<<"\n\n";
        imshow("edges", edges);
        
        if(waitKey(30) >= 0) break;
    }
    

    
    return 0;
}